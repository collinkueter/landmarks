//
//  ContentView.swift
//  Landmarks
//
//  Created by Collin Kueter on 6/29/19.
//  Copyright © 2019 Collin Kueter. All rights reserved.
//

import SwiftUI

struct ContentView : View {
    var body: some View {
        Text("Hello World")
    }
}

#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
#endif
